package INF102.lab5.graph;

import java.util.HashSet;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
        // implement the connected method
        if (!graph.hasNode(u) || !graph.hasNode(v)) {
            throw new IllegalArgumentException("Node not in graph");
        }
        HashSet<V> visited = new HashSet<>();
        return search(u, v, visited);
    }

    private boolean search(V u, V v, HashSet<V> visited) {
        //implement the search method
        if (u.equals(v)) {
            return true;
        }
        visited.add(u);
        for (V n : graph.getNeighbourhood(u)) {
            if (!visited.contains(n)) {
                if (search(n, v, visited)) {
                    return true;
                }
            }
        }
        return false;
    }

}
